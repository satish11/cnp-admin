FROM centos/nodejs-6-centos7

WORKDIR /usr/src/site
VOLUME /usr/src/site
COPY package.json ./


COPY . /usr/src/site


EXPOSE 5002

WORKDIR /usr/src/site


CMD ["npm","run","dev"]


